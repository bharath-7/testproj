package com.example.myapplication

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.util.Log


class SmsReceiver : BroadcastReceiver() {

    private val TAG: String = SmsReceiver::class.java.getSimpleName()
    val pdu_type = "pdus"


    @TargetApi(Build.VERSION_CODES.M)
    override fun onReceive(
        context: Context?,
        intent: Intent
    ) {
        val bundle = intent.extras
        val msgs: Array<SmsMessage?>
        var strMessage = ""
        var text = ""
        val format = bundle!!.getString("format")
        // Retrieve the SMS message received.
        val pdus = bundle[pdu_type] as Array<Any>?
        if (pdus != null) {
            val isVersionM = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            // Fill the msgs array.
            msgs = arrayOfNulls(pdus.size)
            for (i in msgs.indices) {
                if (isVersionM) {
                    msgs[i] = SmsMessage.createFromPdu(
                        pdus[i] as ByteArray,
                        format
                    )
                } else {
                    msgs[i] =
                        SmsMessage.createFromPdu(pdus[i] as ByteArray)
                }
                // Build the message to show.
                strMessage += "SMS from " + msgs[i]?.originatingAddress
                strMessage += " :" + (msgs[i]?.messageBody) + "\n"
                text += (msgs[i]?.messageBody).toString()

                //Check for message source
                if (msgs[i]?.originatingAddress.toString().equals("VM-GUDSMS")) {
                    Log.e("TAG", text.substring(text.length - 4, text.length))
                    val intent = Intent()
                    intent.action = "com.example.Broadcast"

                //User would possibly be receiving a lot of messages from us, the same source. We could use a simple regex to sort it out.
                    intent.putExtra("MyData", text.substring(text.length - 4, text.length))
                    context!!.sendBroadcast(intent)

                }
            }
        }
    }


}