package com.example.myapplication

import `in`.aabhasjindal.otptextview.OtpTextView
import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


class BlankFragment : Fragment(), TheBroadcastListener {

    val PERMISSIONS_RECEIVE_SMS = 12574
    val PERMISSIONS_REQUEST_READ_SMS = 15487
    val TAG = "my app"

    lateinit var theBR: TheBroadcastListener

    private lateinit var thisContext: Context
    private var otpTextView: OtpTextView? = null
    private var receiver: BroadcastReceiver? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_blank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        theBR = this
        otpTextView = view.findViewById(R.id.otp_view)
        checkPermision()

    }

    override fun onResume() {
        super.onResume()

        // Can be configured with shared prefs to determine first use and toast accordingly
        if (ContextCompat.checkSelfPermission(
                thisContext,
                Manifest.permission.READ_SMS
            ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                thisContext,
                Manifest.permission.RECEIVE_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(context, "Please provide permission to use the app.", Toast.LENGTH_LONG).show()

        } else {
            Toast.makeText(context, "Permission Granted.", Toast.LENGTH_LONG).show()
            configureReceiver()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        thisContext = context
    }

    private fun checkPermision() {

//        Batch permissions seemed to be troublesome, so i've requested both the requests separately, on after the other

        if (ContextCompat.checkSelfPermission(
                thisContext,
                Manifest.permission.RECEIVE_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "not granted")
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity!!,
                    Manifest.permission.RECEIVE_SMS
                )
            ) {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    PERMISSIONS_RECEIVE_SMS
                )
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    PERMISSIONS_RECEIVE_SMS
                )
            }
        } else {
//            Toast.makeText(context, "Permission Granted.", Toast.LENGTH_LONG).show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        context?.unregisterReceiver(receiver)
    }


    private fun configureReceiver() {
        val filter = IntentFilter()
        filter.addAction("com.example.Broadcast")
        receiver = MyReceiver(theBR) //constructor params to call the interface implemented method
        context?.registerReceiver(receiver, filter)
    }

    // interface to update the UI upon receiving the intent from the broadcast
     override fun updateUI(value: String) {
        otpTextView?.setOTP(value)
    }


    class MyReceiver() : BroadcastReceiver() {

        lateinit var listener: TheBroadcastListener

        constructor(theBR: TheBroadcastListener?) : this() {
            listener = theBR!!
        }

        override fun onReceive(context: Context?, intent: Intent?) {
            val otp = intent?.getStringExtra("MyData")
            this.listener.updateUI(otp!!)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_RECEIVE_SMS -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d(TAG, "PERMISSIONS_RECEIVE_SMS permission granted")
                    if (ContextCompat.checkSelfPermission(
                            activity!!,
                            Manifest.permission.READ_SMS
                        )
                        != PackageManager.PERMISSION_GRANTED
                    ) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                activity!!,
                                Manifest.permission.READ_SMS
                            )
                        ) {

                        } else {
                            ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.READ_SMS),
                                PERMISSIONS_REQUEST_READ_SMS
                            )
                        }
                    } else {
//                        Log.e(TAG, " permission granted")
                        // Permission has already been granted
                    }


                } else {
                    // permission denied
//                    Log.e(TAG, "PERMISSIONS_RECEIVE_SMS permission denied")
                }
                return
            }

            PERMISSIONS_REQUEST_READ_SMS -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                    Log.e(TAG, "PERMISSIONS_REQUEST_READ_SMS permission granted")
                } else {
//                    Log.e(TAG, "PERMISSIONS_REQUEST_READ_SMS permission denied")
                }
                return
            }
            else -> {
            }
        }
    }


}



