package com.example.myapplication

public interface TheBroadcastListener {

    fun updateUI(value: String)

}